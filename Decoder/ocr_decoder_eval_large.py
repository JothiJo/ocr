'''OCR decoder'''
import cv2
from SimpleHTR.simple_htr_large import SimpleHtrOcrEngine
import pandas as pd
import difflib as dfb
import time
import editdistance

class ocr_decoder():
    def __init__(self,modelDir,charListDir,product_listpath,threshold=0.5):
        '''Initialise model dir and Reference prodid list path'''
        productids = pd.read_excel(product_listpath)
        productids = productids['MaterialNo'].tolist()
        self.similarity_threshold = threshold
        self.productids =[str(x) for x in productids]
        self.ocr_engine = SimpleHtrOcrEngine(modelDir,charListDir)
 
    def ocr_postprocess(self):
        '''find the closest match of decoder output with product id list'''
        productids = self.productids
        text = self.decoded_text.upper()
        text = text.replace('O','0')
        output=[]
        results = []
        result_score = []
        difflibtag =[]
        st=time.time()
        best_match = dfb.get_close_matches(text, productids)
        for i in range(len(best_match)):
            score = dfb.SequenceMatcher(None,text,best_match[i]).ratio()
            if score > self.similarity_threshold :
                    results.append(best_match[i])
                    result_score.append(score)
                    difflibtag.append(True)
        print(time.time()-st)
                
        if len(results)==0:
            output = [[text],[0.0],[False]]
        else:
            best_score = max(result_score)
            top_results = [results[x] for x in range(len(result_score)) if result_score[x]==best_score]
            top_score = [result_score[x] for x in range(len(result_score)) if result_score[x]==best_score]
            top_difftag = [difflibtag[x] for x in range(len(result_score)) if result_score[x]==best_score]
            output = [top_results,top_score,top_difftag]
                
        return output
    
    def decode(self,image):
        '''Getting result from ocr decoder'''
        decoded_text = self.ocr_engine.predict(image)
        # decoded_text = ''.join(e for e in decoded_text if e.isalnum())
        self.decoded_text =decoded_text
        result = self.ocr_postprocess()
        return result
 

def main():
    model_Dir = '/media/recode/DATA2/ocr_train/SimpleHTR-master_large/model_wrng'
    charListpath = '/media/recode/DATA2/ocr_train/SimpleHTR-master_large/model_wrng/charList.txt'
    productidlistpath = '/media/recode/DATA2/ocr/ocr_Decoder/ocr/Decoder/SimpleHTR/Item_Master Data 05.12.2019.xlsx'
    import os
    decoder = ocr_decoder(model_Dir,charListpath,productidlistpath,threshold=0.95)
    traindata = pd.read_excel('/media/recode/DATA2/ocr_train/intrain.xlsx')
    traindata = traindata['data'].tolist()
    traindata = [str(x) for x in traindata]
    inppath = '/media/recode/DATA2/ocr_train/Test_pid'
    
    Testcrctpath ='/media/recode/DATA2/ocr_train/SimpleHTR-master_large/Testcrt_pid/'
    Testwrngpath ='/media/recode/DATA2/ocr_train/SimpleHTR-master_large/Testwrng_pid/'
    files = os.listdir(inppath)
    valid_area = lambda h, w: h * w > 250
    valid_aspect_ratio = lambda h, w: w / h > 2
    output=[]
    numWordOK =0
    numWordTotal=0
    numCharErr=0
    numCharTotal=0
    for img in files:
        if img.split('_')[0] not in traindata:
            imname = os.path.join(inppath, img)
            gt = img.split('_')[0]
            image = cv2.imread(imname)
            h,w ,ch= image.shape
            if valid_area(h, w) and valid_aspect_ratio(h, w):
                # cv2.imwrite(Testpath+img,image)
                try:
                    result = decoder.decode(image)
                except:
                     print('none')
                out=''
                crctflg = 0
                for i in range(len(result[0])):
                    if i==len(result[0])-1:
                        out=out + result[0][i]
                    else:
                        out=out + result[0][i]+'|'
                    if gt == result[0][i]:
                        crctflg=1  
                        break
                        
                if crctflg == 1:
                    # crctflg=1
                    numWordOK += 1 
                    cv2.imwrite(Testcrctpath+img,image)
                    
                else:
                    numWordOK += 0
                    cv2.imwrite(Testwrngpath+img,image)
                    
                numWordTotal += 1
                
                # dist = editdistance.eval(result[0][i], gt)
                # numCharErr += dist
                numCharTotal += len(gt)
                output.append(img+'|'+gt+'|' +out)
                
                print('output : ',result)
                print('finished..')
    charErrorRate = numCharErr / numCharTotal
    wordAccuracy = numWordOK / numWordTotal
    print('charErrorRate : ',charErrorRate)
    print('wordAccuracy : ',wordAccuracy)
       
    f=open('/media/recode/DATA2/ocr_train/scripts/output_tightall_large_pid.txt','w')
    for ele in output:
        f.write(ele+'\n')
    f.close()

if __name__ == '__main__':
    main()