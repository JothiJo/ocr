'''OCR decoder'''
import os
import cv2
import time
import numpy as np
# from ocr_decoder.simple_htr.simple_htr import SimpleHtrOcrEngine
from simple_htr.simple_htr import SimpleHtrOcrEngine
import pandas as pd
import difflib as dfb


class ocr_decoder():
    def __init__(self, modelDir, charListDir, product_id_list, threshold=0.5):
        '''Initialise model dir and Reference prodid list path'''
        # product_id_list = pd.read_excel(product_listpath)
        # product_id_list = product_id_list['MaterialNo'].tolist()
        self.similarity_threshold = threshold
        self.productids = [str(x) for x in product_id_list]
        self.ocr_engine = SimpleHtrOcrEngine(modelDir, charListDir)

    def ocr_postprocess(self):
        '''find the closest match of decoder output with product id list'''
        productids = self.productids
        # text = self.decoded_text.upper()
        text = self.decoded_text
        output = []
        results = []
        result_score = []
        difflibtag = []
        best_match = dfb.get_close_matches(text, productids)
        for i in range(len(best_match)):
            score = dfb.SequenceMatcher(None, text, best_match[i]).ratio()
            if score > self.similarity_threshold:
                results.append(best_match[i])
                result_score.append(score)
                difflibtag.append(True)

        if len(results) == 0:
            output = [[text], [0.0], [False]]
        else:
            best_score = max(result_score)
            top_results = [results[x] for x in range(len(result_score)) if result_score[x] == best_score]
            top_score = [result_score[x] for x in range(len(result_score)) if result_score[x] == best_score]
            top_difftag = [difflibtag[x] for x in range(len(result_score)) if result_score[x] == best_score]
            output = [top_results, top_score, top_difftag]

        return output

    def decode(self, image):
        '''Getting result from ocr decoder'''
        decoded_text, probability = self.ocr_engine.predict(image)
        # self.decoded_text = decoded_text.upper()
        # result = self.ocr_postprocess()
        # return result
        return decoded_text.upper(), probability

def main():
    model_Dir = '/media/Work/Codes/Private/ocr/Decoder/model_v4'
    charListpath = '/media/Work/Codes/Private/ocr/Decoder/model_v4/charList.txt'
    productidlistpath = '/media/Work/Codes/Private/ocr/Decoder/SimpleHTR/Item_Master Data 05.12.2019.xlsx'
    image_folder = '/media/Work/Codes/Private/ocr/Decoder/data'

    product_id_list = pd.read_excel(productidlistpath)
    product_id_list = product_id_list['MaterialNo'].tolist()

    decoder = ocr_decoder(model_Dir, charListpath, product_id_list, threshold=0.8)
    img_sample = np.zeros((100, 100), dtype=np.uint8)
    start = time.time()
    result = decoder.decode(img_sample)
    print(f'Time Duration: {time.time() - start}')
    images = []
    for image_name in os.listdir(image_folder):
        image_path = os.path.join(image_folder, image_name)
        images.append(cv2.imread(image_path, 0))

    images = images * 10
    for image in images:
        start = time.time()
        result = decoder.decode(image)
        print(f'Time Duration: {time.time() - start}')
        print('output : ', result)
    print('finished..')


if __name__ == '__main__':
    main()
