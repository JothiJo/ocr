'''OCR decoder'''
import os
import cv2
import time
import numpy as np
# from ocr_decoder.simple_htr.simple_htr import SimpleHtrOcrEngine
from simple_htr.simple_htr import SimpleHtrOcrEngine
import pandas as pd


class ocr_decoder():
    def __init__(self, modelDir, charListDir):
        '''Initialise model dir and Reference prodid list path'''
        self.ocr_engine = SimpleHtrOcrEngine(modelDir, charListDir)

    def decode(self, images):
        '''Getting result from ocr decoder'''
        decoded_text, probability = self.ocr_engine.predict(images)
        return list(map(str.upper, decoded_text)), probability

def main():
    model_Dir = '/media/Work/Codes/Private/ocr/Decoder/model_v4'
    charListpath = '/media/Work/Codes/Private/ocr/Decoder/model_v4/charList.txt'
    productidlistpath = '/media/Work/Codes/Private/ocr/Decoder/SimpleHTR/Item_Master Data 05.12.2019.xlsx'
    # image_folder = '/media/Work/Codes/Private/ocr/Decoder/data'
    image_folder = '/media/Work/Codes/End2End/dws/old_text/02_04_2021_cleaned_output/11-04-2021/09.00.00/09.11.33.325_cf391aec-9a77-11eb-940d-024220601393/OtherTexts'

    decoder = ocr_decoder(model_Dir, charListpath)
    img_sample = np.zeros((100, 100), dtype=np.uint8)
    start = time.time()
    result = decoder.decode([img_sample])
    print(f'Time Duration: {time.time() - start}')
    images = []
    for image_name in os.listdir(image_folder):
        image_path = os.path.join(image_folder, image_name)
        images.append(cv2.imread(image_path, 0))

    # images = images*320
    images = images[:258]
    # for image in images:

    start = time.time()
    result = decoder.decode(images)
    print(f'Time Duration: {time.time() - start}')
    print('output : ', result[0])
    print('finished..')


if __name__ == '__main__':
    main()
