'''OCR decoder'''
import cv2
from SimpleHTR.simple_htr import SimpleHtrOcrEngine
import pandas as pd
import difflib as dfb
import time


class ocr_decoder():
    def __init__(self, modelDir, charListDir, product_listpath, threshold=0.5):
        '''Initialise model dir and Reference prodid list path'''
        productids = pd.read_excel(product_listpath)
        productids = productids['MaterialNo'].tolist()
        self.similarity_threshold = threshold
        self.productids = [str(x) for x in productids]
        self.ocr_engine = SimpleHtrOcrEngine(modelDir, charListDir)

    def ocr_postprocess(self):
        '''find the closest match of decoder output with product id list'''
        productids = self.productids
        text = self.decoded_text.upper()
        output = []
        results = []
        result_score = []
        difflibtag = []
        st = time.time()
        best_match = dfb.get_close_matches(text, productids)
        for i in range(len(best_match)):
            score = dfb.SequenceMatcher(None, text, best_match[i]).ratio()
            if score > self.similarity_threshold:
                results.append(best_match[i])
                result_score.append(score)
                difflibtag.append(True)
        print(time.time() - st)

        if len(results) == 0:
            output = [[text], [0.0], [False]]
        else:
            best_score = max(result_score)
            top_results = [results[x] for x in range(len(result_score)) if result_score[x] == best_score]
            top_score = [result_score[x] for x in range(len(result_score)) if result_score[x] == best_score]
            top_difftag = [difflibtag[x] for x in range(len(result_score)) if result_score[x] == best_score]
            output = [top_results, top_score, top_difftag]

        return output

    def decode(self, image):
        '''Getting result from ocr decoder'''
        decoded_text = self.ocr_engine.predict(image)
        self.decoded_text = decoded_text
        result = self.ocr_postprocess()
        return result


import numpy as np


def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")
    # apply gamma cor1/0.5rection using the lookup table
    return cv2.LUT(image, table)


def main():
    model_Dir = '/media/recode/DATA2/ocr_train/SimpleHTR-master/model'
    charListpath = model_Dir + '/charList.txt'
    productidlistpath = '/media/recode/DATA2/ocr/ocr_Decoder/ocr/Decoder/SimpleHTR/Item_Master Data 05.12.2019.xlsx'
    imname = '/media/recode/DATA2/ocr_train/Testwrng/ADDRESS_02220.jpg'
    decoder = ocr_decoder(model_Dir, charListpath, productidlistpath, threshold=0.85)
    image = cv2.imread(imname, 0)
    # image = image/255
    # imagere = cv2.resize(image,(300,100))
    # # kernel = np.ones((2,2), np.uint8)
    # adjusted = adjust_gamma(image, gamma=0.9)
    for j in range(1):
        cc = time.time()
        result = decoder.decode(image)
        print('tottime', time.time() - cc)
    print('output : ', result)
    print('finished..')


if __name__ == '__main__':
    main()
